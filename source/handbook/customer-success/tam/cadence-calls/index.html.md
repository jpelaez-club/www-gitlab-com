---
layout: handbook-page-toc
title: "Cadence Calls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

## What is a Cadence Call?

One of the primary tools TAMs have to become a trusted advisor and assess and improve account [health](/handbook/customer-success/tam/health-score-triage/) is the customer cadence call. This is an opportunity for the TAM and the customer team to sync on business outcomes, priorities, progress on initiatives, and concerns, and it is a great opportunity to bring in other GitLab team members that the TAM feels should be included (for example, [Product](/handbook/customer-success/tam/product/) to review feature requests and the roadmap).

Cadence calls are an important aspect of TAM engagement to continue to understand customer's evolving needs, ensure GitLab is delivering value and outcomes, unblock barriers, address issues, collect feedback, nurture relationships, and ensure the customer continues to have positive experiences.

### Frequency

- **Enterprise**: Cadence calls should be weekly during onboarding and at least once per month otherwise, considering specific customer needs and stage in customer lifecycle.
- **Commercial**: Cadence calls should be weekly during onboarding and at least once per quarter otherwise, considering specific customer needs and stage in customer lifecycle.

### Cadence Call Notes

In addition to Timeline notes in Gainsight, call notes should be [saved in Google Drive](https://drive.google.com/drive/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U), following this format: `/Sales/Customers & Prospects/A/Acme/Acme - Meeting Notes`. [See an example meeting notes here](https://docs.google.com/document/d/1dAcHBqoRTY6qqSw27VQstCCnk5Fxc2oIsbpKs014h3g).

Meanwhile, when TAMs are logging calls in Gainsight, they need to ensure to copy & paste the link to the Google Doc with a simple summary of the meeting, so that it is reflected in the Gainsight [Timeline](/handbook/customer-success/tam/gainsight/timeline/) but not duplicating their efforts.

The rationale for saving call notes in this manner is as follows:

- The naming convention ""`Customer` - Meeting Notes" allows for fast searching using [Google Cloud Search for Work](https://cloudsearch.google.com/)
- Call notes frequently contain sensitive information and are for the internal sales team and management to review and should be kept in a place everyone who might need access can find them.
- A folder structure allows non-Customer Success executives and support staff to easily locate the notes in the case of an escalation.
- Call notes are tightly linked to the [health score](/handbook/customer-success/tam/health-score-triage/) and should be available for reference in the same location as the health scorecard in Gainsight.
- Access to Gainsight is limited to TAMs, so other members of the Sales and Customer Success organizations will look for notes in Google Drive.

At the end of each customer call any changes to customer health should be reflected in the customer's Gainsight account. You have a few ways to update the TAM Sentiment and Product Sentiment for an account's health score, described in [Determining TAM Sentiment and Product Risk](/handbook/customer-success/tam/health-score-triage/#determining-tam-sentiment-and-product-risk), the easiest of which is updating it directly when logging the call.

### Cadence Call Topics

The below non-exhaustive list is simply suggestions for cadence calls, and other topics may be more important, so use the suggestions at your discretion.

There are two sections, [General Suggestions](/handbook/customer-success/tam/cadence-calls/#general-suggestions) is for topics that are good at any time, and [Ephemeral Suggestions](/handbook/customer-success/tam/cadence-calls/#ephemeral-suggestions) is for topics that are ephemeral, such as release-specific topics or requests from Product Managers.

#### General Suggestions

These are suggestions that can be used at any time on customer calls.

- Are you running the most recent version / patch of gitlab?
- Have you recently tested restoring from a backup?
- Are signups enabled? Do you want them to be?
- Review upcoming features / releases
- Discuss planned upgrades and if they need [Live Upgrade Assistance](/support/scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance)
- Stage adoption questions
- Discovery questions about their usage and processes
- Do your users have any feedback?
- Are there any areas for enablement or training?
- How are you leveraging other tools / integrations?
- What are your best practices and typical workflows?

#### Ephemeral Suggestions

These are suggestions that can be used on customer calls, but are time-sensitive. Examples would be deprecated features, requests for information from Product, upcoming release highlights, etc.

- PostgreSQL update
- Only/Except deprecation
- Release 13.0 highlights
