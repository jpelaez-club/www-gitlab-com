---
layout: markdown_page
title: "Category Direction - Continuous Delivery"
---
 
- TOC
{:toc}
 
## Continuous Delivery
 
Many teams are reporting that development velocity is stuck; they've
reached a plateau in moving to more and more releases per month, and now need help on how to improve. According to analyst research, 40% of software development
team's top priorities relate to speed/automation, so our overriding vision
for Continuous Delivery is to help these teams renew their ability to
accelerate delivery.
 
Additionally, Continuous Delivery serves as the "Gateway to Operations"
for GitLab, unlocking the downstream features such as the [Configure](/direction/ops/#configure)
and [Monitor](/direction/ops/#configure) stages.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Delivery)
- [Overall Vision](/direction/ops/#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=Category%3AContinuous%20Delivery)
- [Research insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AContinuous%20Delivery)
- [Documentation](https://docs.gitlab.com/ee/ci/)
 
### Continuous Delivery vs. Deployment
 
We follow the well-known definitions from Martin Fowler on the
difference between continuous delivery and continuous deployment:
 
- **Continuous Delivery** is a software development discipline
 where you build software in such a way that the software can
 be released to production at any time. 
- **Continuous Deployment** means that every change goes through the
 pipeline and automatically gets put into production, resulting in many
 production deployments every day.
In order to do Continuous Deployment you must be doing Continuous Delivery.
 
_Source: [https://martinfowler.com/bliki/ContinuousDelivery.html](https://martinfowler.com/bliki/ContinuousDelivery.html)_
 

### Infrastructure Provisioning
 
Infrastructure Provisioning and Infrastructure as Code, using solutions like Terraform or other provider-specific methods, is an interesting topic that relates to deployments but is not part of the Continuous Delivery category here at GitLab. For details on solutions GitLab provides in this space, take a look at the [category page](/direction/configure/infrastructure_as_code/) for our Infrastructure as Code team.
 
### Deployment with Auto DevOps
 
For deployment to Kubernetes clusters, GitLab has a focused category called Auto DevOps which is oriented around providing solutions for deploying to Kubernetes. Check out their [category page](/direction/configure/auto_devops/) for details on what they have planned.

We are working on a similar experience for non Kubernetes users, starting with [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351) that will automatically detect when users are deploying to AWS and will connect the dots for them.

The [Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/#auto-deploy) [jobs](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib%2Fgitlab%2Fci%2Ftemplates%2FJobs) within [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) are maintained by the Continuous Delivery category.
 
## What's Next & Why

[Progressive Delivery](https://about.gitlab.com/direction/ops/#progressive-delivery) is a main pillar in Continuous Delivery, and one that we are investing lots of efforts in. 
We want to extend our support and discoverability of [Advanced deployments](https://gitlab.com/groups/gitlab-org/-/epics/2213), and are currently documenting how you can use Blue/Green deplyments with GitLab via [gitlab#14763](https://gitlab.com/gitlab-org/gitlab/-/issues/14763).

We have recently delivered several features related to our [Natively supporting hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804) theme and specifically deploying to [AWS](https://gitlab.com/groups/gitlab-org/-/epics/2351). In order to help users find these resources more easily, we are adding in-product guidance via [gitlab#207830](https://gitlab.com/gitlab-org/gitlab/issues/207830).


## Maturity Plan
 
This category is currently at the "Complete" maturity level, and our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:
 
 **CI.yaml features**
- [Limit pipeline concurrency using named semaphores](https://gitlab.com/gitlab-org/gitlab/issues/15536) (Complete)
- [Group deploy tokens](https://gitlab.com/gitlab-org/gitlab/issues/21765) (Complete)
- [Allow only forward deployments](https://gitlab.com/gitlab-org/gitlab/issues/25276)  (Complete)
- [Group Deploy Keys](https://gitlab.com/gitlab-org/gitlab/issues/14729)
- [Allow fork pipelines to run in parent project](https://gitlab.com/groups/gitlab-org/-/epics/3278)

**Advanced Deployments**
- [Advanced Deploys](https://gitlab.com/groups/gitlab-org/-/epics/2213)

**Cloud Deployments**
- [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351)

**Observability**
- [Post-deployment monitoring MVC](https://gitlab.com/groups/gitlab-org/-/epics/3088)
- [Actionable CI/CD metrics](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838)
 
## Competitive Landscape
 
Because CI and CD are closely related, the [competitive analysis for Continuous Integration](/direction/verify/continuous_integration#competitive-landscape)
is also relevant here. For how CD compares to other products in the market,
especially as it relates to pipelines themselves, also take a look there.
 
As far as CD specifically, Microsoft has always been strong in providing actionable metrics on
development, and as they move forward to integrate GitHub and Azure
DevOps they will be able to provide a wealth of new metrics to help
teams using their solution. We can stay in front of this by improving
our own actionable delivery metrics in the product via [gitlab-ee#7838](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838).

### Harness

Harness is a modern, cloud-native CD platform
that provides excellent solutions for delivering to cloud environments
using native approaches like Kubernetes. Additionally, it manages the deployment all the way through
to monitoring, which we will introduce via [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295).
 We are planning to conduct research on [Harness](https://gitlab.com/gitlab-org/gitlab/-/issues/20307) and invite you to chime in on the issues and provide your insights and feedback as well.
 
### Spinnaker
 
Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes.
It combines a powerful and flexible pipeline management system with integrations to the major cloud providers and treats deployments as a first class citizen. However,
it also has a weakness that it does not support other stages of the DevOps lifecycle (such as Continuous Integration), while GitLab offers a single tool for your Development needs. 

Our goal is to do make it so anything that Spinnaker can do, can also be done via built-in features in GitLab CD.

Spinnaker's advantage points are: 
* Provides easy application deployment across cloud providers. We have also recognised this as one of our top vision items and are working to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804).
* Supports several deployment strategies which we are working on expanding in [Advanced Deploys](https://gitlab.com/groups/gitlab-org/-/epics/2213).
* Allows configuring an environment in standby for easy rollback, which is closely tied to [gitlab#35409](https://gitlab.com/gitlab-org/gitlab/issues/35409).
* Provides a user interface that allows you to view your infrastructure and to see exactly where your code is residing.  
* Acts as an operator that perform rollback when needed without the need to provide a script to do so (similar to Kubernetes). 
* Ablility to manage [multiple k8s clusters](https://docs.gitlab.com/ee/user/project/clusters/#multiple-kubernetes-clusters-premium), which is also supported by GitLab.


One analysis of GitLab vs. Spinnaker can be found on our [product comparison page](/devops-tools/spinnaker-vs-gitlab.html). There is another high quality one from our VP of product strategy in [gitlab#197709](https://gitlab.com/gitlab-org/gitlab/issues/197709#note_274765321). Finally, our PM for this section completed one where which can be found at [gitlab#35219](https://gitlab.com/gitlab-org/gitlab/issues/35219). 
Our next step is to review these and combine in this section of this document as the single source of truth, exhaustively listing the issues we plan to deliver to reach functional parity (or better) with Spinnaker.


We also respect our customers choice to use Spinnaker's CD solution together with GitLab and are working on making
that integration easier with [gitlab#120085](https://gitlab.com/gitlab-org/gitlab/issues/120085).
 
## Analyst Landscape
 
In our conversations with industry analysts, there are a number of key trends
we're seeing happening in the CD space:

### Support a breadth of platforms, both legacy and cloud-native.

Cloud adoption of CI/CD is growing, extending capabilities for deploying to cloud environments, including Kubernetes and other modern container
architectures are a key metric. While cloud migration is accelerating and more teams are adopting it, on-premises and
legacy hardware environments remain. 
 
We invite you to follow our plans to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804) and  [Serverless](https://about.gitlab.com/direction/configure/serverless/) to offer feedback or ask questions.
 
### Inject insight and analytics into pipelines.
 
Users are looking for the ability to not just measure platform stability and other
performance KPIs post-deployment, but also providie functionality such as automated release-readiness scoring
based on analysis of data from across the digital pipelines. 
Tracking and measuring customer behavior, experience, and financial impact, after deployment via [gitlab#37139](https://gitlab.com/gitlab-org/gitlab/issues/37139) solves an important
pain point.

### Progressive Delivery

Progressive Delivery is an approach to deliver application and service functionality incrementally, to targeted user segments and production environments, limiting the blast radius, enabling experimentation with reduced risk. Progressive Delivery builds on the foundations laid by Continuous Delivery/Continuous Integration.
To read more about this see [RedMonk's post](https://redmonk.com/jgovernor/2019/07/10/progressive-delivery-at-gitlab/).


## Top Customer Success/Sales Issue(s)
 
The ability to monitor deployments and automatically halt/rollback deployment in case of exceeding a specific error rate is frequently mentioned by CS and in the sales cycle as a feature teams are looking for. This will be
implemented via [gitlab#3088](https://gitlab.com/groups/gitlab-org/-/epics/3088). We will start by showing alert on the environments page, in case an error threshold is crossed in [gitlab#214634](https://gitlab.com/gitlab-org/gitlab/-/issues/214634) and will iterate on this by adding a button that will allow you to cancel the deployment via [gitlab#216949](https://gitlab.com/gitlab-org/gitlab/-/issues/216949). 
 
## Top Customer Issue(s)
 
Our most popular customer request is [gitlab#24309](https://gitlab.com/gitlab-org/gitlab/-/issues/24309), which adds notifications for when pipelines are fixed. If a pipeline becomes fails, an email is sent as notification. However, if that pipeline becomes succeeds again, no update is sent. In order to know, a person would have to proactively go into the web experience to check. This is not an effective use of their time - if we think it's important enough to send an email when they break, it is important enough to send an email when restored.

## Top Internal Customer Issue(s)
 
Allowing forked pipelines to run in a parent project via [gitlab#3278](https://gitlab.com/groups/gitlab-org/-/epics/3278) is our most popular intrnal customer issue. It will especially help us verify that community contributions live peacefully with our tests before merging them. We will start solving this with [gitlab#217451](https://gitlab.com/gitlab-org/gitlab/-/issues/217451) which will allow forked pipelines to create a pipeline in the parent project but only a user with appropriate permission in the parent's project will be able to run this and merge it.

## Top Vision Item(s)
 
Our top vision item is to [Natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804), and specifically deploying to [AWS](https://gitlab.com/groups/gitlab-org/-/epics/2351) we want to help make it easier and quicker to get started and deploy to any one of the big cloud providers using GitLab's CI/CD.
