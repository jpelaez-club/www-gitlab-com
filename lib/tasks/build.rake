# NOTE: This is only intended to be used for local development/testing.  From the .gitlab-ci.yml,
#       we invoke the middleman build commands directly, so that build failures have simpler stacktraces
#       that aren't wrapped in the Rake stack.
namespace :build do
  desc "Build the entire site, including all sub-sites"
  task :all do
    # Build top level
    puts "Building top level..."
    system(build_cmd) || raise("command failed: #{build_cmd}")

    # Build sub-sites
    monorepo_config = YAML.load_file(File.expand_path('../../data/monorepo.yml', __dir__))
    sites = monorepo_config.keys
    sites.each do |site|
      build_site(site)
    end
  end

  desc "Build sites/handbook site"
  task :handbook do
    build_site(:handbook)
  end

  def build_cmd
    "NO_CONTRACTS=#{ENV['NO_CONTRACTS'] || 'true'} middleman build --bail"
  end

  def build_site(site)
    site_dir = File.expand_path("../../sites/#{site}", __dir__)
    Dir.chdir(site_dir) do
      puts "\n\nBuilding '#{site}' site from #{site_dir}..."
      system("#{build_cmd} --no-clean") || raise("command failed for '#{site}' site in #{site_dir}: #{build_cmd}")
    end
  end
end
